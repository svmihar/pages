---
title: yet another knowledge management
layout: post
categories: [produktivitas]
date: 2020-01-19
description: in what format does your brain save memories? is it by index or value?

---

> zettelkasten
![](https://i.imgur.com/eB4Z4Is.png)

---

## intro
agak setuju sih dengan perkataanya om noam chomsky, bahasa merupakan representasi ilmu pada manusia[^1]. seandainya nanti kita bertemu dengan mahluk intelejen selain homo sapiens, bagaimana cara kita berkomunikasi? walaupun a picture worth a thousand words, in the end information is words.

## history
inget sekali jaman dulu pas mau masuk kuliah masih pake celana panjang warna abu-abu, dimana akhirnya belajar punya tujuan selain dapat nilai bagus, dan manjangin rambut. buku catatan saya jaman dulu tidak boleh ada garis, (dengan segala kenaifan jaman dulu), buku dengan garis itu menghambat kreatifitas saya untuk membuktikan rumus atau menggambar grafik. selain itu saya juga menganut satu buku untuk semua,  jadi gak perlu ribet membeda-bedakan buku catatan (bawa buku diktat dari sekolah itu udah berat, saya juga orangnya males). semua pelajaran di enkapsulasi di satu buku. hasilnya? susahnya mengingat informasi, gak usah mengingat mencari informasi simple seperti satu rumus aja sulit, atau cari catetan pribadi tentang suatu hal aja lama sekali. dari scribblings sampai pembuktian rumus tetap jadi satu. jaman sma saya juga suka ngoprek android, windows,  ala-ala bikin template website untuk jualan, mulailah mengenal google keep, dari to-do-list sampe template tulisan di forum jual beli ada di dalam situ.

masuk kuliah tetap dengan kemalasan yang sama satu buku untuk semua matkul. tetapi dengan apprroach menulis yang cukup berbeda. jadi daripada "meringkas" apa yagn di dapat dari narasumber, saya anggap buku itu adalah sebuah cheatsheet, atau orang IT jaman sekarang sebagai *key-value pair*[^2]. percayalah metode ini sampah dan hanya berguna ketika mau quiz doang. setelah semester 7, awal 2018, dimana saya mulai menyerah dengan segala pembuktian secara analisis, serta aljabar yang lebih menuju ke aljubur, juga proyekan-proyekan dosen yang lebih banyak ke arah ngoding, membuat menulis di buku **irelevan**. disini catatan digital saya banyak berpindah, mulai dari proyekan video yang ditulis to-do-list nya ke dalam google keep, sampai pindahin semua catatan proyekan ke dalam evernote sampai jam 2.30 pagi juga pernah.

masuklah ke jaman mengenal [notion](https://www.notion.so/). semua berawal dari temen kampus yang punya semacam time-table untuk mengatur segala macam notes serta project management nya. dari design process nya notion ini klop sekali dengan prinsip saya. one book to rule them all. proses migrasi mulai dari catatan kuliah, referensi TA, ringkasan paper masuk semua ke notion. gratis, tapi ada limit sampai 500MB saya pikir ah tak masalah, notion checks it all
1. punya link youtube video? *embed*.
3. ada artikel blog menarik tentang perbedaan algortima sorting? masukkin *save it for later* notion.
4. lagi nyari switch keyboard yang paling bagus, terus bikin perbandingan nya di tabel tapi males bikin *spreadsheet*? masuk notion.
5. punya laporan hasil eksperimen dan bisa *edit kolektif realtime* kayak google docs? notion lah!
6. *sync* dari gambar dari hp ke komputer di dalam page notion? ez.

ah *semua untuk notion, notion untuk semua*.

*memang semakin lama menggunakan suatu sistem pada akhirnya akan membenci sistem itu juga*[^4]. permasalahannya simple, bagaimana cara menulis rumus dengan segala macam alfabet yunani nya tanpa latex / mathjax / katex? selain itu mudahnya "copas" artikel tanpa diproses terlebih dahulu membuat notion lebih kepada bank sampah daripada bank ilmu. seolah-olah kembali lagi di masa SMA. banyaknya informasi yang ada di dalem notion, serta tech stack notion yagn menggunakan NoSQL + Electron itu gak cocok dengan pengguna laptop kacrut yang buka video youtube 1080p 60fps aja bikin rpm fan nya 100% serasa pesawat terbang mau take off. bayangkan hanya untuk mencari sebuah keyword dalam ribuan dokumen butuh waktu 10 detik.

tidak teratur, serta bebasnya membuat catatan itu sendiri membuat konsep belajar jadi tidak berguna. masa-masa ini adalah dimana saya sedang gandrung-gandrungnya baca buku. buku yang saya baca sering saya ringkas untuk diingat kembali, dan sadar kalau mencatat di buku itu jauh lebih lama ingetnya ketimbang hanya sekedar select -> hightlight -> paste ke notion. ibaratnya **active recall** itu emang penting untuk gathering knowledge.


## current state
berawal dari sebuah graph network, namanya pagerank. kalau boleh tak (over)simplify, bagaimana menilai sebuah catatan yang penting? frekuensi berkunjung, bagaimana cara google membuat index untuk seluruh laman website yagn di crawl untuk mendapatkan "nilai" agar mudah di ranking? dengan tags. dari sini saya mulai senang mengeksplor all thins graph, lalu dapatlah sebuah note taking tool (based on the graph network) namanya [roam research](https://roamresearch.com/), kayanya mudah banget ngelink satu page dengan page lainnya. semua interconnected,dengan cara memecah sebuah informasi ke bentuk paling dasar. atau istilah matematikanya bikin pohon, lebih tepatnya merkle tree. jadi untuk menjawab masalah searching, daripada bikin memecah menurut kategori, lebih baik pisah secara komponen. **tidak perlu ada hierarki, yang penting koneksi ada antar notes.**
![](http://www.shadowandy.net/wp/wp-content/uploads/merkle-tree.png)
![](https://roamresearch.com/assets/images/Roam-Group-min.png)

### roam research atau foam?
sayang beribu sayang, roam research ini bayar, dan lebih horornya subscription based. seperti yang diajarkan seluruh ibu-ibu di asia tenggara, kalau ada yang gratis ngapain bayar[^3]? enter [foam research](https://foambubble.github.io/foam/). **vim, markdown dan open source** is currently my jam. bayangkan, bikin interlink antar kata hanyalah sebatas menulis `[[ sebuah tutup kurung ]]` ditambah lagi bisa menggunakan vim untuk navigasi? match vim untuk navigasi? match made in heaven. dengan begini seluruh catatan saya dapat berupa markdown, di sync dari github dan dropbox, serta punya tampilan website dengan bikin static website generator seperti nikola / mkdocs supaya bisa melihat gambar-gambarnya. seluruh box yang ada di notion tercentang, seluruh fitur yang ada di roam research pun didapat, yang dikorbankan tentu saja hanyalah waktu untuk migrasi dan energi untuk mendeploy nya ke github.

## parting words
pentingnya punya "second brain", itu adalah hal yang wajib. apalagi di tempat dimana orangnya harus punya ilmu yang terus update dan tidak melupakan elemen klasik, vim, markdown, dan foam serta catatan dibuat dengan "hierarki" zettelkasten adalah my "current" perfect match.
![](https://i.imgur.com/yrScLYf.png)
![](https://foambubble.github.io/foam/assets/images/foam-features-dark-mode-demo.png)

---
referensi
[^1]: [Chomsky–Schützenberger enumeration theorem](https://en.wikipedia.org/wiki/Chomsky%E2%80%93Sch%C3%BCtzenberger_enumeration_theorem)
[^2]: [mapping](https://docs.python.org/3/library/stdtypes.html#typesmapping)
[^3]: [prinsip ekonomi](https://www.zonareferensi.com/prinsip-ekonomi/)
[^4]: [diambil dari buku unscripted - mj demarco](http://www.getunscripted.com/)