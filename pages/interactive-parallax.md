---
title: interactive parallax
layout: project
---

<canvas class="canvas" width="1080" height="1920" id="canvas"></canvas>

<script>
export const data = {
  title: "project1",
  layout: "page",
};
export default {
  props: ["page"],
  mounted() {
    let js = document.createElement("script");
    js.setAttribute(
      "src",
      "/main.js"
    );
    document.head.appendChild(js);
  },
};
</script>

<style></style>
